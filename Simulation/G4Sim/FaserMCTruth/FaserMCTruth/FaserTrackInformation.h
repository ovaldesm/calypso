/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FaserTrackInformation_H
#define FaserTrackInformation_H

#include "FaserVTrackInformation.h"

namespace ISF {
  class FaserISFParticle;
}

class FaserTrackInformation: public FaserVTrackInformation {
public:
	FaserTrackInformation();
	FaserTrackInformation(const HepMC::GenParticle*,const ISF::FaserISFParticle* baseIsp=0);
	const HepMC::GenParticle *GetHepMCParticle() const;
	const ISF::FaserISFParticle *GetBaseISFParticle() const;
	int GetParticleBarcode() const;
	void SetParticle(const HepMC::GenParticle*);
	void SetBaseISFParticle(const ISF::FaserISFParticle*);
	void SetReturnedToISF(bool returned) {m_returnedToISF=returned;};
	bool GetReturnedToISF() const {return m_returnedToISF;};
	void SetRegenerationNr(int i) {m_regenerationNr=i;};
	int GetRegenerationNr() const {return m_regenerationNr;};
private:
	int m_regenerationNr;
	const HepMC::GenParticle *m_theParticle;
	const ISF::FaserISFParticle *m_theBaseISFParticle;
	bool m_returnedToISF;
};

#endif
