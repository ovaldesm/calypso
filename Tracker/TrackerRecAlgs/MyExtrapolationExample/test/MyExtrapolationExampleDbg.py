#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log
from AthenaCommon.Constants import DEBUG, VERBOSE
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from MyExtrapolationExample.MyExtrapolationExampleConfig import MyExtrapolationExampleCfg


log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure
ConfigFlags.Input.Files = ['my.HITS.pool.root']
ConfigFlags.Output.ESDFileName = "MyExtrapolationExample.ESD.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(MyExtrapolationExampleCfg(ConfigFlags))
acc.getEventAlgo("Tracker::MyExtrapolationExample").OutputLevel = VERBOSE

sc = acc.run(maxEvents=10)
sys.exit(not sc.isSuccess())
