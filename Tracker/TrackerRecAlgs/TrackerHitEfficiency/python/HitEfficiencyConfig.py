from AthenaConfiguration.ComponentFactory import CompFactory
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg


def HitEfficiencyCfg(flags, **kwargs):
    acc = FaserSCT_GeometryCfg(flags)
    HitEfficiencyAlg = CompFactory.HitEfficiencyAlg
    acc.addEventAlgo(HitEfficiencyAlg(**kwargs))

    thistSvc = CompFactory.THistSvc()
    thistSvc.Output += ["HIST1 DATAFILE='HitEfficiency.root' OPT='RECREATE'"]
    acc.addService(thistSvc)
    return acc