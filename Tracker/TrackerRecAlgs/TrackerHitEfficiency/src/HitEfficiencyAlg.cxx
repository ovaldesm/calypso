#include "HitEfficiencyAlg.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include <TTree.h>
#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "TrackerReadoutGeometry/SCT_DetectorManager.h"

constexpr double MeV2GeV = 1e-3;
constexpr float NaN = std::numeric_limits<float>::quiet_NaN();


HitEfficiencyAlg::HitEfficiencyAlg(const std::string& name, ISvcLocator* pSvcLocator) :
    AthReentrantAlgorithm(name, pSvcLocator), AthHistogramming(name), m_histSvc("THistSvc/THistSvc", name) {}

StatusCode HitEfficiencyAlg::initialize() {
  ATH_CHECK(m_trackCollectionKey.initialize());

  ATH_CHECK(m_clusterContainerKey.initialize());

  ATH_CHECK(detStore()->retrieve(m_sctHelper, "FaserSCT_ID"));

  ATH_CHECK(detStore()->retrieve(m_detMgr, "SCT"));

  // ATH_CHECK(m_trackingGeometryTool.retrieve());

  m_tree = new TTree("tree", "tree");
  m_tree->Branch("run", &m_run, "run/I");
  m_tree->Branch("event", &m_event, "event/I");
  m_tree->Branch("chi2", &m_chi2, "chi2/D");
  m_tree->Branch("ndf", &m_ndf, "ndf/D");
  m_tree->Branch("nClusters", &m_nClusters, "nClusters/I");
  m_tree->Branch("station", &m_station, "station/I");
  m_tree->Branch("layer", &m_layer, "layer/I");
  m_tree->Branch("side", &m_side, "side/I");
  m_tree->Branch("phi", &m_phi, "phi/I");
  m_tree->Branch("moduleON", &m_moduleON, "moduleON/I");
  m_tree->Branch("eta", &m_eta, "eta/I");
  m_tree->Branch("x", &m_x, "x/D");
  m_tree->Branch("y", &m_y, "y/D");
  m_tree->Branch("z", &m_z, "z/D");
  m_tree->Branch("px", &m_px, "px/D");
  m_tree->Branch("py", &m_py, "py/D");
  m_tree->Branch("pz", &m_pz, "pz/D");
  m_tree->Branch("tanTheta", &m_tanTheta, "tanTheta/D");
  m_tree->Branch("insideElement", &m_insideElement, "insideElement/O");
  m_tree->Branch("residual", &m_residual, "residual/D");
  m_tree->Branch("radius", &m_radius, "radius/D");
  m_tree->Branch("phi_clust", &m_phi_clust);
  m_tree->Branch("eta_clust", &m_eta_clust);
   
  ATH_CHECK(histSvc()->regTree("/HIST/tree", m_tree));
  
  return StatusCode::SUCCESS;
}

int HitEfficiencyAlg::moduleON(const int eta, const int phi) const {
  if (eta == 1 && phi == 3)
    return 0;
  if (eta == 1 && phi == 2)
    return 1;
  if (eta == 1 && phi == 1)
    return 2;
  if (eta == 1 && phi == 0)
    return 3;
  if (eta == -1 && phi == 3)
    return 4;
  if (eta == -1 && phi == 2)
    return 5;
  if (eta == -1 && phi == 1)
    return 6;
  if (eta == -1 && phi == 0)
    return 7;
  else return -1;
}

// check if hit is in any of the eight detector elements in the plane defined by the station, layer and side
bool HitEfficiencyAlg::hitsDetectorElement(const Amg::Vector3D &position, int station, int layer, int side) const {
  for (const TrackerDD::SiDetectorElement *element : *m_detMgr->getDetectorElementCollection()) {
    Identifier id = element->identify();
    if (m_sctHelper->station(id) == station && m_sctHelper->layer(id) == layer && m_sctHelper->side(id) == side) {
      const auto& bounds = element->bounds();
      if (bounds.inside(element->localPosition(position), m_tolerance, m_tolerance)) {
        return true;
      }
    }
  }
  return false;
}

StatusCode HitEfficiencyAlg::execute(const EventContext &ctx) const {
  // Check if the masked layer is set
  if (m_maskedStation == -1) {
    ATH_MSG_ERROR("You have to set the station for which the efficiency should be calculated.");
    return StatusCode::FAILURE;
  }
  if (m_maskedLayer == -1) {
    ATH_MSG_ERROR("You have to set the layer for which the efficiency should be calculated.");
    return StatusCode::FAILURE;
  }
  if (m_maskedSide == -1) {
    ATH_MSG_ERROR("You have to set the side for which the efficiency should be calculated.");
    return StatusCode::FAILURE;
  }
  m_run = ctx.eventID().run_number();
  m_event = ctx.eventID().event_number();

  // std::map<Identifier, const FaserSiHit*> siHitMap;

  SG::ReadHandle<TrackCollection> trackCollection {m_trackCollectionKey, ctx};
  ATH_CHECK(trackCollection.isValid());
  
  SG::ReadHandle<Tracker::FaserSCT_ClusterContainer> clusterContainer {m_clusterContainerKey, ctx};
  ATH_CHECK(clusterContainer.isValid());
  m_station = m_maskedStation.value();
  m_layer = m_maskedLayer.value();
  m_side = m_maskedSide.value();

  int nTracks {0};
  int nClusters {0};

  for (const Trk::Track *track : *trackCollection) {
    if (!track) continue;
    nTracks++;
  }

  std::set<int> stationMap;
  //std::cout << "First iteration of track collection" << std::endl;
  for (const Trk::Track* track : *trackCollection)
  {
    if (track == nullptr) continue;
    if (nTracks != 1) continue;
    // Check for hit in the three downstream stations
    int cnt_measurementsOnTrack=0;
    for (auto measurement : *(track->measurementsOnTrack())) {
        cnt_measurementsOnTrack++;
        const Tracker::FaserSCT_ClusterOnTrack* cluster = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*>(measurement);
        if (cluster != nullptr){
	    nClusters++;
            Identifier id = cluster->identify();
            int station = m_sctHelper->station(id);
            stationMap.emplace(station);
	    //std::cout << "First Check: Station = "  << station << std::endl;
        }
    }
    //std::cout<<"cnt_measurementsOnTrack = "<<cnt_measurementsOnTrack<<std::endl;   
  }
  //std::cout<<"First iteration end"<<std::endl;
  std::cout<<"nTracks = "<<nTracks<<", nClusters = "<<nClusters<<std::endl;
  std::cout<<"stationMap.count"<<((int)(stationMap.count(0)))<<" - "<<((int)(stationMap.count(1)))<<" - "<<((int)(stationMap.count(2)))<<" - "<<((int)(stationMap.count(3)))<<std::endl;
  //if ((nTracks == 1) && (stationMap.count(0) != 0 && stationMap.count(1) != 0 && stationMap.count(2) != 0 && stationMap.count(3) != 0)){
  if ((nTracks == 1) && (stationMap.count(m_station) != 0)){
    //std::cout << "selected" << std::endl;
    //std::cout << "Second iteration of track collection" << std::endl;
    clearTree();
    for (const Trk::Track *track : *trackCollection) {
      if (!track) continue;
      int cnt_TrackStateOnSurface = 0;
      int cnt_TrackStateOnSurface_valid = 0;
      std::vector<double> residuals, abs_residuals;
      m_chi2 = track->fitQuality()->numberDoF() == 0 ? -1 : track->fitQuality()->chiSquared() / track->fitQuality()->numberDoF();
      m_ndf = track->fitQuality()->doubleNumberDoF();
      bool m_pass = true;
      m_nClusters = 0;
      for (const Trk::TrackStateOnSurface *state : *track->trackStateOnSurfaces()) {
        cnt_TrackStateOnSurface++;
        const Trk::MeasurementBase *measurement = state->measurementOnTrack();
        auto clusterOnTrack = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*> (measurement);
        if (!clusterOnTrack) continue;
        cnt_TrackStateOnSurface_valid++;
        Identifier id = clusterOnTrack->identify();
        if ( m_sctHelper->station(id) == m_station ) {
          m_nClusters += 1;
        }
        if (m_pass == false) continue;
        // select only the track in the extrapolation layer (and side) nearest to the masked layer
	//std::cout << "Second Check: Station = "  << ((int)(m_sctHelper->station(id))) << ", Layer = " << ((int)(m_sctHelper->layer(id))) << ", Side = " << ((int)(m_sctHelper->side(id))) << std::endl; 
	if ( m_sctHelper->station(id) == m_station && m_sctHelper->layer(id) == m_layer && m_sctHelper->side(id) != m_side ) {
          m_phi = m_sctHelper->phi_module(id);
          m_eta = m_sctHelper->eta_module(id);
          m_moduleON = moduleON(m_eta,m_phi);
          Amg::Vector3D pos = state->trackParameters()->position();
          m_insideElement = hitsDetectorElement(pos, m_sctHelper->station(id), m_sctHelper->layer(id), m_sctHelper->side(id));
          m_x = pos.x();
          m_y = pos.y();
          m_z = pos.z();
          Amg::Vector3D mom = state->trackParameters()->momentum();
          m_px = mom.x();
          m_py = mom.y();
          m_pz = mom.z();
          m_tanTheta = std::sqrt(m_px * m_px + m_py * m_py) / m_pz;
          m_radius = std::sqrt(m_x * m_x + m_y * m_y);
          m_pass = false;
        }
      }
       //std::cout << "cnt_TrackStateOnSurface = " << cnt_TrackStateOnSurface << ", cnt_TrackStateOnSurface_valid = " << cnt_TrackStateOnSurface_valid << std::endl;   
      if (m_pass == true) continue;
      // check if there are additional hits compatible with the track
      for (const auto& clusterCollection : *clusterContainer) {
        Identifier clusterID = clusterCollection->identify();
        if (m_sctHelper->station(clusterID) == m_station && m_sctHelper->layer(clusterID) == m_layer && m_sctHelper->side(clusterID) == m_side) {
          m_phi_clust.push_back(m_sctHelper->phi_module(clusterID));
          m_eta_clust.push_back(m_sctHelper->eta_module(clusterID));
          for (const auto& cluster : *clusterCollection) {
            auto pos_cluster = cluster->localPosition();
            auto glob_pos_cluster = cluster->globalPosition();
            double m_x_ext, m_y_ext, m_y_ext_cor, m_rot;
            m_x_ext = m_x + (glob_pos_cluster.z() - m_z) * (m_px / m_pz);
            m_y_ext = m_y + (glob_pos_cluster.z() - m_z) * (m_py / m_pz);
            // m_rot = (m_phi + (m_eta + 1) / 2 + m_side) % 2 == 0 ? 20e-3 : -20e-3;
            m_rot = cluster->detectorElement()->sinStereo();
            m_y_ext_cor = m_y_ext - std::tan(m_rot) * (glob_pos_cluster.x() - m_x_ext);
            residuals.push_back(glob_pos_cluster.y() - m_y_ext_cor);
          }
        }
      }
      // minimal residual between track position and cluster
      if (!residuals.empty()){
        for (auto res : residuals){
          abs_residuals.push_back(fabs(res));
        }
      }
      m_residual = residuals.empty() ? -1 : residuals.at(std::distance(abs_residuals.begin(), std::min_element(abs_residuals.begin(), abs_residuals.end())));
      m_tree->Fill();
    }
     //std::cout<<"Second iteration end"<<std::endl;
  } 

  return StatusCode::SUCCESS;
}

StatusCode HitEfficiencyAlg::finalize() {
  return StatusCode::SUCCESS;
}

void HitEfficiencyAlg::clearTree() const {
  m_x = NaN;
  m_y = NaN;
  m_z = NaN;
  m_px = NaN;
  m_py = NaN;
  m_pz = NaN;
  m_phi = -1;
  m_eta = 0;
  m_moduleON = -1;
  m_chi2 = NaN;
  m_ndf = NaN;
  m_nClusters = 0;
  m_tanTheta = NaN;
  m_insideElement = false;
  m_residual = NaN;
  m_radius = NaN;
  m_phi_clust.clear();
  m_eta_clust.clear();
}
