#ifndef HITEFFICIENCYALG_H
#define HITEFFICIENCYALG_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "AthenaBaseComps/AthHistogramming.h"
#include "GaudiKernel/ServiceHandle.h"
#include "TrkTrack/TrackCollection.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
// #include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"


class ISvcLocator;
class StatusCode;
class TTree;
class FaserSCT_ID;
namespace Trk { class TrackStateOnSurface; }
namespace  TrackerDD { class SCT_DetectorManager; }


class HitEfficiencyAlg : public AthReentrantAlgorithm, AthHistogramming {
public:
  HitEfficiencyAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  const ServiceHandle<ITHistSvc>& histSvc() const;

private:
  void clearTree() const;
  ServiceHandle<ITHistSvc> m_histSvc;
  SG::ReadHandleKey<TrackCollection> m_trackCollectionKey { this, "TrackCollection", "CKFTrackCollection"}; //4st, not reliable
  //SG::ReadHandleKey<TrackCollection> m_trackCollectionKey { this, "TrackCollection", "SegmentFit"}; 
  //SG::ReadHandleKey<TrackCollection> m_trackCollectionKey { this, "TrackCollection", "CKFTrackCollectionWithoutIFT"}; 
  //SG::ReadHandleKey<TrackCollection> m_trackCollectionKey { this, "TrackCollection", "CKFTrackCollectionBackwardWithoutIFT"};
  SG::ReadHandleKey<Tracker::FaserSCT_ClusterContainer> m_clusterContainerKey { this, "FaserSCT_ClusterContainer", "SCT_ClusterContainer", "cluster container"};
  // ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool {this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
  const FaserSCT_ID* m_sctHelper;
  const TrackerDD::SCT_DetectorManager* m_detMgr {nullptr};
  int moduleON(const int eta, const int phi) const;
  double residual(const std::vector<double> &p1, const Amg::Vector3D &p2, const int side, const int moduleON) const;
  IntegerProperty m_maskedStation {this, "MaskedStation", -1, "Station for which the efficiency is calculated"};
  IntegerProperty m_maskedLayer {this, "MaskedLayer", -1, "Layer for which the efficiency is calculated"};
  IntegerProperty m_maskedSide {this, "MaskedSide", -1, "Side for which the efficiency is calculated"};
  DoubleProperty m_tolerance {this, "Tolerance", -3.0 * Gaudi::Units::mm, "Tolerance if track hits detector element"};
  bool hitsDetectorElement(const Amg::Vector3D &position, int station, int layer, int side) const;

  mutable TTree* m_tree;
  
  mutable int m_run;
  mutable int m_event;
  mutable double m_chi2;
  mutable double m_ndf;
  mutable int m_nClusters=0;
  mutable int m_station;
  mutable int m_layer;
  mutable int m_side;
  mutable int m_phi;
  mutable int m_eta;
  mutable int m_moduleON;
  mutable double m_x;
  mutable double m_y;
  mutable double m_z;
  mutable double m_px;
  mutable double m_py;
  mutable double m_pz;
  mutable double m_tanTheta;
  mutable bool m_insideElement;
  mutable double m_residual;
  mutable double m_radius;
  mutable std::vector<int> m_phi_clust;
  mutable std::vector<int> m_eta_clust;
};
  

inline const ServiceHandle<ITHistSvc>& HitEfficiencyAlg::histSvc() const {
  return m_histSvc;
}

#endif //HITEFFICIENCYALG_H
