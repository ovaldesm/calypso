#include "TrkDetStripClusterImporter/StripClusterImporter.h"
#include "xAODFaserTracking/StripClusterAuxContainer.h"

namespace TrkDet {

  StripClusterImporter::StripClusterImporter (const std::string& name, ISvcLocator* pSvcLocator) : 
    AthAlgorithm( name, pSvcLocator ),
    m_clusCollectionName("StripClusters")
  {
      declareProperty("OutputClustersName"                    , m_clusCollectionName);
  }

  StripClusterImporter::~StripClusterImporter()
  {}

  StatusCode StripClusterImporter::initialize() {

    return StatusCode::SUCCESS;
  }

  StatusCode StripClusterImporter::finalize()
  {
    return StatusCode::SUCCESS;
  }

  StatusCode StripClusterImporter::retrieveFatrasCluster() const
  {
    const std::vector<float> *g_x,*g_y,*g_z,*l_x,*l_y;
    CHECK( evtStore()->retrieve( g_x, "g_x" ) );
    CHECK( evtStore()->retrieve( g_y, "g_y" ) );
    CHECK( evtStore()->retrieve( g_z, "g_z" ) );
    CHECK( evtStore()->retrieve( l_x, "l_x" ) );
    CHECK( evtStore()->retrieve( l_y, "l_y" ) );
    // Add one strip cluster to the container:
    
    // Create the cluster containers:
    xAOD::StripClusterContainer* clusterC = new xAOD::StripClusterContainer();
    ATH_CHECK( evtStore()->record( clusterC, m_clusCollectionName ) );
    xAOD::StripClusterAuxContainer* aux = new xAOD::StripClusterAuxContainer();
    ATH_CHECK( evtStore()->record( aux, m_clusCollectionName + "Aux." ) );
    clusterC->setStore( aux );  
    //p.setLocalPosition( 1.0, 2.0);
    //p.setLocalPositionError( 0.2, 0.2, 0.05 );
    
    for(unsigned int i = 0; i<g_x->size();i++){
        xAOD::StripCluster* p = new xAOD::StripCluster();
        clusterC->push_back( p );
        p->setId(i);
        float x(g_x->at(i)),y(g_y->at(i)),z(g_z->at(i));
        float lx(l_x->at(i)),ly(l_y->at(i));
        p->setGlobalPosition( z, y, x );
        p->setLocalPosition(lx, ly);
        //implement hack for covariance here!
        //p->setLocalPositionError(covX, covY, covXY);
        ATH_MSG_DEBUG( "global x = " << p->globalX() << ", global y = " << p->globalY() << ", global z = " << p->globalZ() );
    }
    
    
    return StatusCode::SUCCESS;
  }


  StatusCode StripClusterImporter::execute() {

    if(!retrieveFatrasCluster()){
        return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }

}