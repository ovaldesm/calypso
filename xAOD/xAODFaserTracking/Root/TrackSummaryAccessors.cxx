// System include(s):
extern "C" {
#   include <stdint.h>
}
#include <iostream>

// Local include(s):
#include "xAODFaserTracking/FaserTrackSummaryAccessors.h"

/// Helper macro for Accessor objects
#define DEFINE_ACCESSOR(TYPE, NAME )                               \
   case xAOD::NAME:                                                \
   {                                                               \
      static SG::AuxElement::Accessor< TYPE > a( #NAME );          \
      return &a;                                                   \
   }                                                               \
   break;

namespace xAOD {

  // Generic case. Maybe return warning?
  template<class T>
   SG::AuxElement::Accessor< T >*
   faserTrackSummaryAccessor( xAOD::FaserSummaryType /*type*/ )
   {}

  template<>
   SG::AuxElement::Accessor< uint8_t >*
   faserTrackSummaryAccessor<uint8_t>( xAOD::FaserSummaryType type ) {

      switch( type ) {
        DEFINE_ACCESSOR( uint8_t, numberOfContribStripLayers        );
        DEFINE_ACCESSOR( uint8_t, numberOfStripHits                   );
        DEFINE_ACCESSOR( uint8_t, numberOfStripOutliers               );
        DEFINE_ACCESSOR( uint8_t, numberOfStripHoles                  );
        DEFINE_ACCESSOR( uint8_t, numberOfStripDoubleHoles            );
        DEFINE_ACCESSOR( uint8_t, numberOfStripSharedHits             );
        DEFINE_ACCESSOR( uint8_t, numberOfStripDeadSensors            );
        DEFINE_ACCESSOR( uint8_t, numberOfStripSpoiltHits             );
     
        DEFINE_ACCESSOR( uint8_t, numberOfOutliersOnTrack           );
        DEFINE_ACCESSOR( uint8_t, standardDeviationOfChi2OS         );
      default:                  
         std::cerr << "xAOD::Track ERROR Unknown FaserSummaryType ("
                   << type << ") requested" << std::endl;
         return 0;
      }
   } 
} // namespace xAOD
